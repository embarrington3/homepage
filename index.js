var clockElement = document.getElementById("clock");

function clock() {
  var date = new Date();
  var seconds = date.getSeconds().toString().padStart(2,0);
  var minutes = date.getMinutes().toString().padStart(2,0);
  var hours = date.getHours().toString().padStart(2,0);
  var period = "";
  if (hours >= 12) {
    period = "PM";
  } else {
    period = "AM";
  }

  // if (hours > 12) {
  //   hours -= 12;
  // }

  clockElement.textContent =
    hours + ":" + minutes + ":" + seconds + " " + period;
}
document.onload = setInterval(clock, 0);

document.getElementById("input").addEventListener("keydown", (e) => {
  const value = document.getElementById("input").value;
  // const url = "https://baresearch.org/search?q=";
  const url = "https://www.google.com/search?q=";

  if (e.keyCode == 13) {
    if (
      value.endsWith(".com") ||
      value.endsWith(".edu") ||
      value.endsWith(".gov") ||
      value.endsWith(".net") ||
      value.endsWith(".tv") ||
      value.endsWith(".io") ||
      value.endsWith(".org") ||
      value.endsWith(".uk") ||
      value.endsWith(".ca") ||
      value.endsWith(".au") ||
      value.endsWith(".de") ||
      value.endsWith(".ru") ||
      value.endsWith(".ir") ||
      value.endsWith(".mil") ||
      value.endsWith(".us") ||
      value.endsWith(".dev") ||
      value.endsWith(".market") ||
      value.endsWith(".cn") || 
      value.endsWith(".cc") ||
      value.endsWith(".co") |
      value.endsWith(".xyz") ||
      value.endsWith(".ly") ||
      value.endsWith(".info") ||
      value.endsWith(".int")  ||
      value.endsWith(".gg")
    ) {
      const urlBase = "https://";
      window.location.href = urlBase + value;
    } else {
      window.location.href = url + value;
    }
  }
});
