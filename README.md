<div align="center">
<h1> Start Page</h1>

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/embarrington3/homepage?branch=master&style=for-the-badge)

<h2> Built With:</h2>

![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)

<h2> License </h2>

![GitLab](https://img.shields.io/gitlab/v/license/embarrington3/homepage?style=for-the-badge)

<h2> Image </h2>
<img src="./images/screenshot.png" align="center">

</div>


## Planned Features

- [ ] Weather
- [ ] Theme swap 
- [x] Hover css 
- [ ] Stock information
- [ ] Clock w/ different time zones 
- [ ] Loading css
- [ ] Storage local
- [ ] Configurable setings menu
- [ ] Search engine selector
- [ ] Scale to different devices
- [ ] Url handling